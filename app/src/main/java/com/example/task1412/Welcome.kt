package com.example.task1412
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_welcome.*

class Welcome : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        var intent = intent
        var name = intent.getStringExtra("username")
        usernametextViewwelcome.text = "username: "+name+""
    }
}