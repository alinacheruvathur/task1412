package com.example.task1412

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        submitLoginButton.setOnClickListener() {
            val username = this.usernameLoginEdittext.getText().toString()
            var intent = Intent(this, Welcome::class.java)
            intent.putExtra("username",username)

            if (usernameLoginEdittext.getText().toString().equals("user") &&(passwordLoginEdittext.getText().toString().equals("000000"))) {
                startActivity(intent)
            }
            else{

                Toast.makeText(applicationContext,"wrong username or password",Toast.LENGTH_SHORT).show()
            }
        }
    }
}